import React, { Component } from "react";
import {
    Platform,
    View,
    ScrollView,
    Text,
    StatusBar,
    SafeAreaView,
    TouchableOpacity,
    FlatList,
    Image
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { sliderWidth, itemWidth } from "app/styles/SliderEntry.style";
import SliderEntry from "app/components/SliderEntry";
import styles, { colors } from "app/styles/index.style";
import { ENTRIES1, ENTRIES2, ENTRIES3 } from "app/static/entries";
import { scrollInterpolators, animatedStyles } from "app/utils/animations";
import CarouselView from 'react-native-looped-carousel';
import stylesTest from "app/stylesTest";

const IS_ANDROID = Platform.OS === "android";
const SLIDER_1_FIRST_ITEM = 1;

export default class example extends Component {
    constructor(props) {
        super(props);
        this.state = {
            slider1ActiveSlide: SLIDER_1_FIRST_ITEM
        };
        this.indexPrev = 0;
        this.indexBackup = 0;
        this.indexBackupBeing = 0;
        this.moveToRight = false;
    }

    _renderItem({ item, index }) {
        return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
    }

    _renderItemWithParallax({ item, index }, parallaxProps) {
        return (
            <SliderEntry
                data={item}
                even={(index + 1) % 2 === 0}
                parallax={true}
                parallaxProps={parallaxProps}
            />
        );
    }

    _renderLightItem({ item, index }) {
        return <SliderEntry data={item} even={false} />;
    }

    _renderDarkItem({ item, index }) {
        return <SliderEntry data={item} even={true} />;
    }

    mainExample(number, title) {
        const { slider1ActiveSlide } = this.state;

        return (
            <View style={styles.exampleContainer}>
                <Text style={styles.title}>{`Example ${number}`}</Text>
                <Text style={styles.subtitle}>{title}</Text>
                <Carousel
                    ref={c => (this._slider1Ref = c)}
                    data={ENTRIES1}
                    renderItem={this._renderItemWithParallax}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    hasParallaxImages={true}
                    firstItem={SLIDER_1_FIRST_ITEM}
                    inactiveSlideScale={0.94}
                    inactiveSlideOpacity={0.7}
                    // inactiveSlideShift={20}
                    containerCustomStyle={styles.slider}
                    contentContainerCustomStyle={styles.sliderContentContainer}
                    loop={true}
                    loopClonesPerSide={2}
                    autoplay={true}
                    autoplayDelay={500}
                    autoplayInterval={3000}
                    onSnapToItem={index =>
                        this.setState({ slider1ActiveSlide: index })
                    }
                />
                <Pagination
                    dotsLength={ENTRIES1.length}
                    activeDotIndex={slider1ActiveSlide}
                    containerStyle={styles.paginationContainer}
                    dotColor={"rgba(255, 255, 255, 0.92)"}
                    dotStyle={styles.paginationDot}
                    inactiveDotColor={colors.black}
                    inactiveDotOpacity={0.4}
                    inactiveDotScale={0.6}
                    carouselRef={this._slider1Ref}
                    tappableDots={!!this._slider1Ref}
                />
            </View>
        );
    }

    momentumExample(number, title) {
        return (
            <View style={styles.exampleContainer}>
                <Text style={styles.title}>{`Example ${number}`}</Text>
                <Text style={styles.subtitle}>{title}</Text>
                <Carousel
                    data={ENTRIES2}
                    renderItem={this._renderItem}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    inactiveSlideScale={0.95}
                    inactiveSlideOpacity={1}
                    enableMomentum={true}
                    activeSlideAlignment={"start"}
                    containerCustomStyle={styles.slider}
                    contentContainerCustomStyle={styles.sliderContentContainer}
                    activeAnimationType={"spring"}
                    activeAnimationOptions={{
                        friction: 4,
                        tension: 40
                    }}
                />
            </View>
        );
    }

    layoutExample(number, title, type) {
        const isTinder = type === "tinder";
        return (
            <View
                style={[
                    styles.exampleContainer,
                    isTinder
                        ? styles.exampleContainerDark
                        : styles.exampleContainerLight
                ]}
            >
                <Text
                    style={[styles.title, isTinder ? {} : styles.titleDark]}
                >{`Example ${number}`}</Text>
                <Text
                    style={[styles.subtitle, isTinder ? {} : styles.titleDark]}
                >
                    {title}
                </Text>
                <Carousel
                    data={isTinder ? ENTRIES2 : ENTRIES1}
                    renderItem={
                        isTinder ? this._renderLightItem : this._renderItem
                    }
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    containerCustomStyle={styles.slider}
                    contentContainerCustomStyle={styles.sliderContentContainer}
                    layout={type}
                    loop={true}
                />
            </View>
        );
    }

    customExample(number, title, refNumber, renderItemFunc) {
        const isEven = refNumber % 2 === 0;

        // Do not render examples on Android; because of the zIndex bug, they won't work as is
        return !IS_ANDROID ? (
            <View
                style={[
                    styles.exampleContainer,
                    isEven
                        ? styles.exampleContainerDark
                        : styles.exampleContainerLight
                ]}
            >
                <Text
                    style={[styles.title, isEven ? {} : styles.titleDark]}
                >{`Example ${number}`}</Text>
                <Text style={[styles.subtitle, isEven ? {} : styles.titleDark]}>
                    {title}
                </Text>
                <Carousel
                    data={isEven ? ENTRIES2 : ENTRIES1}
                    renderItem={renderItemFunc}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    containerCustomStyle={styles.slider}
                    contentContainerCustomStyle={styles.sliderContentContainer}
                    scrollInterpolator={
                        scrollInterpolators[`scrollInterpolator${refNumber}`]
                    }
                    slideInterpolatedStyle={
                        animatedStyles[`animatedStyles${refNumber}`]
                    }
                    useScrollView={true}
                />
            </View>
        ) : (
            false
        );
    }

    get gradient() {
        return (
            <LinearGradient
                colors={[colors.background1, colors.background2]}
                startPoint={{ x: 1, y: 0 }}
                endPoint={{ x: 0, y: 1 }}
                style={styles.gradient}
            />
        );
    }

    renderLooping() {
        return (
            <Carousel
                ref={ref => (this.carouselRefTest = ref)}
                data={ENTRIES1}
                renderItem={({ item, index }, parallaxProps) => {
                   
                    return (
                    <TouchableOpacity
                        style={{justifyContent: "center", alignItems: "center", width: "100%", backgroundColor: "yellow", padding: 5}}
                        onPress={() =>
                            {
                                // let indexReal = (index%(ENTRIES1.length-1));
                                // console.log("indexReal", indexReal);
                                // this.carouselView.animateToPage(indexReal);
                                // this.carouselRefTest.snapToItem(indexReal, false, false);
                            }
                        }
                    >
                        <Text>{item.title}</Text>
                    </TouchableOpacity>
                )}}
                sliderWidth={sliderWidth}
                sliderHeight={100}
                itemWidth={150}
                hasParallaxImages={true}
                firstItem={0}
                inactiveSlideScale={1}
                inactiveSlideOpacity={0.5}
                inactiveSlideShift={0}
                containerCustomStyle={[
                    // styles.slider,
                    {backgroundColor: "red"}
                ]}
                // contentContainerCustomStyle={styles.sliderContentContainer}
                loop={true}
                loopClonesPerSide={100}
                enableMomentum={true}
                // activeAnimationType={"decay"}
                // autoplay={true}
                // autoplayDelay={500}
                // autoplayInterval={3000}
                onSnapToItem={index => {}
                    // this.setState({ slider1ActiveSlide: index })
                    // console.log("index", index)
                    // this.carouselView.animateToPage(index)
                }
                
                style={{backgroundColor: "red"}}
            />
        );
    }

    render() {
        const example1 = this.mainExample(
            1,
            "Default layout | Loop | Autoplay | Parallax | Scale | Opacity | Pagination with tappable dots"
        );
        const example2 = this.momentumExample(
            2,
            "Momentum | Left-aligned | Active animation"
        );
        const example3 = this.layoutExample(
            3,
            '"Stack of cards" layout | Loop',
            "stack"
        );
        const example4 = this.layoutExample(
            4,
            '"Tinder-like" layout | Loop',
            "tinder"
        );
        const example5 = this.customExample(
            5,
            "Custom animation 1",
            1,
            this._renderItem
        );
        const example6 = this.customExample(
            6,
            "Custom animation 2",
            2,
            this._renderLightItem
        );
        const example7 = this.customExample(
            7,
            "Custom animation 3",
            3,
            this._renderDarkItem
        );
        const example8 = this.customExample(
            8,
            "Custom animation 4",
            4,
            this._renderLightItem
        );

        return (
            <View style={{flex: 1}}>
                <View style={{height: 80}}>
                {this.renderLooping()}
                </View>
                <CarouselView
                ref={ref => (this.carouselView = ref)}
                style={stylesTest.view}
                autoplay={false}
                // pageInfo


                onAnimateNextPage={(p) =>  {
                    
                    // console.log("onAnimateNextPage_indexOld", this.carouselRefTest.currentIndex);
                    // console.log("onAnimateNextPage_indexNew", p);
                    // this.carouselRefTest.snapToNext(false, false);
                    let diff = Math.abs(this.indexBackup - p);
                    let cheat = 0;
                    if( diff == 5 || diff > 1){
                        cheat = this.moveToRight ? 6 : -6;
                    }
                
                    this.carouselRefTest.snapToItem(p + cheat, true, false);

                
                    console.log("onAnimateNextPage", p + " cheat " +  cheat + " indexbu " + this.indexBackup);
                        this.indexBackup = p;

                    
                }}
                onPageBeingChanged={(a) => {
                    if(this.indexBackupBeing == 0 && a == 5){
                        this.moveToRight = false;
                    }else{
                        this.moveToRight =  this.indexBackupBeing < a || (this.indexBackupBeing == 5 && a ==0);
                    }
                    
                    this.indexBackupBeing = a;
                   
                }}
                >
                {
                    ENTRIES1.map((item, index) => 
                        <FlatList
                            style={{ width: "100%"}}
                            key={index}
                            data={ENTRIES3}
                            keyExtractor={(item, index) => index.toString()}
                            ListHeaderComponent={() => <Text>{item.title}</Text>}
                            renderItem={({item}) => <Image source={{uri: item.illustration}} key={item.key} style={{height: 100, width: "100%"}}></Image>}
                            />
                      
                    )
                }
                
                
                </CarouselView>
            </View>
        );
    }
}
