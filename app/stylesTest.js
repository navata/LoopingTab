import { StyleSheet, Dimensions } from 'react-native';
const {width, height }= Dimensions.get('window');

export default StyleSheet.create({
    view: {
        width: width,
        height: height
    }
})