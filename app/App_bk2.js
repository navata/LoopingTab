import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, } from 'react-native';
import InfiniteScroll from "./InfiniteScroll";


export default class App extends Component {
  constructor(props) {
    super(props)
  }
 
  render() {
    return (
      <View style={styles.container}>
        <InfiniteScroll
        data={[{ key: '1' }, { key: '2' }, { key: '33232322322323232' }, { key: '4' }, { key: '5' }, { key: '6' }, { key: '7' }, { key: '8' }, { key: '9' }]}
        renderItem={({ item }) => <View key={item.key} style={styles.listItem}><Text>{item.key}</Text></View>}
 
        />
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 50,
    margin: 2,
    borderColor: '#2a4944',
    borderWidth: 1,
    // backgroundColor: 'red'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
